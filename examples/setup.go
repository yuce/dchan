package examples

import (
	"context"
	"flag"

	"github.com/hazelcast/hazelcast-go-client"
	"github.com/hazelcast/hazelcast-go-client/logger"

	"gitlab.com/yuce/dchan"
)

func CreateSenderReceiver(ctx context.Context, name string) (*dchan.DChan, error) {
	config := hazelcast.Config{}
	config.Logger.Level = logger.WarnLevel
	client, err := hazelcast.StartNewClientWithConfig(ctx, config)
	if err != nil {
		return nil, err
	}
	return dchan.New(ctx, client, name)
}

type Flag struct {
	Distributed bool
	Sender      bool
}

func (f *Flag) Parse() {
	distributed := flag.Bool("d", false, "Enable distributed usage")
	sender := flag.Bool("s", false, "Enable sender")
	flag.Parse()
	f.Distributed = *distributed
	f.Sender = *sender
}
