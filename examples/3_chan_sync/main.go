package main

import (
	"context"
	"fmt"
	"time"

	"github.com/yuce/dchan/examples"
)

func worker(done chan<- interface{}) {
	fmt.Print("working...")
	time.Sleep(time.Second)
	fmt.Println("done")

	done <- true
}

func main() {
	done, err := examples.CreateSenderReceiver(context.Background(), "sample")
	if err != nil {
		panic(err)
	}
	s, err := done.MakeSender(context.TODO())
	if err != nil {
		panic(err)
	}
	r, err := done.MakeReceiver(context.TODO())
	if err != nil {
		panic(err)
	}
	go worker(s)
	<-r
}
