package main

import (
	"context"
	"fmt"
	"time"

	"github.com/yuce/dchan/examples"
)

func main() {
	c1, err := examples.CreateSenderReceiver(context.Background(), "sample")
	if err != nil {
		panic(err)
	}
	c2, err := examples.CreateSenderReceiver(context.Background(), "sample")
	if err != nil {
		panic(err)
	}
	go func() {
		time.Sleep(1 * time.Second)
		c1.S() <- "one"
	}()
	go func() {
		time.Sleep(2 * time.Second)
		c2.S() <- "two"
	}()
	for i := 0; i < 2; i++ {
		select {
		case msg1 := <-c1.R():
			fmt.Println("received", msg1)
		case msg2 := <-c2.R():
			fmt.Println("received", msg2)
		}
	}
}
