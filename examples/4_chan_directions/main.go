package main

import (
	"context"
	"fmt"

	"github.com/yuce/dchan/examples"
)

func ping(pings chan<- interface{}, msg string) {
	pings <- msg
}

func pong(pings <-chan interface{}, pongs chan<- interface{}) {
	msg := <-pings
	pongs <- msg
}

func main() {
	pings, err := examples.CreateSenderReceiver(context.Background(), "sample")
	if err != nil {
		panic(err)
	}
	pongs, err := examples.CreateSenderReceiver(context.Background(), "sample")
	if err != nil {
		panic(err)
	}
	ping(pings.S(), "passed message")
	pong(pings.R(), pongs.S())
	fmt.Println(<-pongs.R())
}
