package main

import (
	"context"
	"fmt"

	"gitlab.com/yuce/dchan/examples"
)

// See: https://gobyexample.com/channels

func sendPing(ch chan interface{}) {
	ch <- "ping"
}

func receivePing(ch chan interface{}) {
	msg := <-ch
	fmt.Println(msg)
}

func mainNonDistributed() {
	messages := make(chan interface{})
	go sendPing(messages)
	receivePing(messages)
}

func mainDistributed(isSender bool) {
	messages, err := examples.CreateSenderReceiver(context.Background(), "hello")
	if err != nil {
		panic(err)
	}
	ch := make(chan interface{})
	if isSender {
		if err := messages.EnhanceSender(context.TODO(), ch); err != nil {
			panic(err)
		}
		sendPing(ch)
		return
	}
	if err := messages.EnhanceReceiver(context.TODO(), ch); err != nil {
		panic(err)
	}
	receivePing(ch)
}

func main() {
	f := examples.Flag{}
	f.Parse()
	if f.Distributed {
		mainDistributed(f.Sender)
		return
	}
	mainNonDistributed()
}
