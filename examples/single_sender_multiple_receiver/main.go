package main

import (
	"context"
	"fmt"
	"sync"

	"gitlab.com/yuce/dchan/examples"
)

func main() {
	ctx := context.Background()
	queue, err := examples.CreateSenderReceiver(ctx, "sample")
	if err != nil {
		panic(err)
	}
	go func() {
		s := make(chan interface{})
		if err := queue.EnhanceSender(context.TODO(), s); err != nil {
			panic(err)
		}
		for i := 0; i < 100; i++ {
			s <- i
		}
		if err := queue.Close(ctx); err != nil {
			panic(fmt.Errorf("closing: %w", err))
		}
		fmt.Println("closed")
	}()
	const receiverCount = 3
	wg := &sync.WaitGroup{}
	wg.Add(receiverCount)
	for i := 0; i < receiverCount; i++ {
		go func(i int) {
			defer wg.Done()
			r := make(chan interface{})
			if err := queue.EnhanceReceiver(context.TODO(), r); err != nil {
				panic(err)
			}
			for elem := range r {
				fmt.Println(i, elem)
			}
		}(i)
	}
	wg.Wait()
}
