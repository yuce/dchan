package main

import (
	"context"
	"fmt"

	"github.com/yuce/dchan/examples"
)

func main() {
	ctx := context.Background()
	jobs, err := examples.CreateSenderReceiver(ctx, "sample")
	if err != nil {
		panic(err)
	}
	done, err := examples.CreateSenderReceiver(ctx, "sample")
	if err != nil {
		panic(err)
	}

	go func() {
		for {
			j, more := <-jobs.R()
			if more {
				fmt.Println("received job", j)
			} else {
				fmt.Println("received all jobs")
				done.S() <- true
				return
			}
		}
	}()

	for j := 1; j <= 3; j++ {
		jobs.S() <- j
		fmt.Println("sent job", j)
	}
	jobs.Close(context.Background())
	fmt.Println("sent all jobs")
	<-done.R()
}
