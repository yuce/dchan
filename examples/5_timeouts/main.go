package main

import (
	"context"
	"fmt"
	"time"

	"github.com/yuce/dchan/examples"
)

func main() {
	c1, err := examples.CreateSenderReceiver(context.Background(), "sample")
	if err != nil {
		panic(err)
	}
	go func() {
		time.Sleep(2 * time.Second)
		c1.S() <- "result 1"
	}()
	select {
	case res := <-c1.R():
		fmt.Println(res)
	case <-time.After(1 * time.Second):
		fmt.Println("timeout 1")
	}
	c2, err := examples.CreateSenderReceiver(context.Background(), "sample")
	if err != nil {
		panic(err)
	}
	go func() {
		time.Sleep(2 * time.Second)
		c2.S() <- "result 2"
	}()
	select {
	case res := <-c2.R():
		fmt.Println(res)
	case <-time.After(3 * time.Second):
		fmt.Println("timeout 2")
	}
}
