package main

import (
	"context"
	"fmt"

	"github.com/yuce/dchan/examples"
)

func main() {
	ctx := context.Background()
	queue, err := examples.CreateSenderReceiver(ctx, "sample")
	if err != nil {
		panic(err)
	}
	go func() {
		s, err := queue.MakeSender(context.TODO())
		if err != nil {
			panic(err)
		}
		s <- "one"
		s <- "two"
		if err := queue.Close(ctx); err != nil {
			panic(fmt.Errorf("closing: %w", err))
		}
		fmt.Println("closed")
	}()
	r, err := queue.MakeReceiver(context.TODO())
	if err != nil {
		panic(err)
	}
	//time.Sleep(5 * time.Second)
	for elem := range r {
		fmt.Println(elem)
	}
}
