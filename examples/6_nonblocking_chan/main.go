package main

import (
	"context"
	"fmt"

	"github.com/yuce/dchan/examples"
)

// See: https://gobyexample.com/non-blocking-channel-operations
// Produces different result, due to buffering.

func main() {
	messages, err := examples.CreateSenderReceiver(context.Background(), "sample")
	if err != nil {
		panic(err)
	}
	signals, err := examples.CreateSenderReceiver(context.Background(), "sample")
	if err != nil {
		panic(err)
	}
	select {
	case msg := <-messages.R():
		fmt.Println("received message", msg)
	default:
		fmt.Println("no message received")
	}
	msg := "hi"
	select {
	case messages.S() <- msg:
		fmt.Println("sent message", msg)
	default:
		fmt.Println("no message sent")
	}
	select {
	case msg := <-messages.R():
		fmt.Println("received message", msg)
	case sig := <-signals.R():
		fmt.Println("received signal", sig)
	default:
		fmt.Println("no activity")
	}
}
