package main

import (
	"context"
	"fmt"

	"gitlab.com/yuce/dchan/examples"
)

// See: https://gobyexample.com/channel-buffering

func sendMessages(messages chan<- interface{}) {
	messages <- "buffered"
	messages <- "channel"
}

func receiveMessages(messages <-chan interface{}) {
	fmt.Println(<-messages)
	fmt.Println(<-messages)
}

func mainNonDistributed() {
	messages := make(chan interface{}, 2)
	sendMessages(messages)
	receiveMessages(messages)
}

func mainDistributed(isSender bool) {
	messages, err := examples.CreateSenderReceiver(context.Background(), "buffered")
	if err != nil {
		panic(err)
	}
	ch := make(chan interface{})
	if isSender {
		if err := messages.EnhanceSender(context.TODO(), ch); err != nil {
			panic(err)
		}
		sendMessages(ch)
	} else {
		if err := messages.EnhanceReceiver(context.TODO(), ch); err != nil {
			panic(err)
		}
		receiveMessages(ch)
	}
}

func main() {
	f := examples.Flag{}
	f.Parse()
	if f.Distributed {
		mainDistributed(f.Sender)
		return
	}
	mainNonDistributed()
}
