package dchan

import "time"

type config struct {
	errorHandler ErrorHandler
	pollTimeout  time.Duration
}

type Option func(c *config) error

func WithErrorHandler(handler ErrorHandler) Option {
	return func(c *config) error {
		c.errorHandler = handler
		return nil
	}
}
