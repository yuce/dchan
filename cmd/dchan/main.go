package main

import (
	"context"
	"flag"
	"log"

	"github.com/hazelcast/hazelcast-go-client"
	"github.com/hazelcast/hazelcast-go-client/logger"
)

func main() {
	prod := flag.Bool("p", false, "set as producer")
	name := flag.String("name", "sample", "set channel name")
	flag.Parse()
	config := hazelcast.Config{}
	config.Logger.Level = logger.WarnLevel
	ctx := context.Background()
	client, err := hazelcast.StartNewClientWithConfig(ctx, config)
	if err != nil {
		log.Fatal(err)
	}
	if *prod {
		// set as producer
		sender, err := dchan.newSender(ctx, client, *name)
		if err != nil {
			log.Fatal(err)
		}
		for _, arg := range flag.Args() {
			log.Println("Sending:", arg)
			sender.Ch() <- arg
		}
		if err = sender.Close(ctx); err != nil {
			panic(err)
		}
	} else {
		recv, err := dchan.newReceiver(ctx, client, *name)
		if err != nil {
			log.Fatal(err)
		}
		for v := range recv.Ch() {
			log.Println("Received:", v)
		}
	}
}
