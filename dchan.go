package dchan

import (
	"context"
	"fmt"
	"log"
	"sync"
	"sync/atomic"
	"time"

	hz "github.com/hazelcast/hazelcast-go-client"
	"github.com/hazelcast/hazelcast-go-client/types"
)

const (
	nameFmt           = "__dchan_%s"
	dchanMap          = "__dchan"
	open        int32 = 0
	closed      int32 = 1
	openValue         = "OPEN"
	closedValue       = "CLOSED"
)

type ErrorHandler func(err error)

type DChan struct {
	client *hz.Client
	name   string
	topic  *hz.Topic
	status int32
	config *config

	mu        *sync.Mutex
	senders   []*sender
	receivers []*receiver
}

func New(ctx context.Context, c *hz.Client, name string, options ...Option) (*DChan, error) {
	config := &config{
		errorHandler: func(err error) {
			log.Printf("ERROR: %s", err.Error())
		},
		pollTimeout: 1 * time.Second,
	}
	dc := &DChan{
		client: c,
		name:   name,
		config: config,
		mu:     &sync.Mutex{},
	}
	for _, opt := range options {
		if err := opt(dc.config); err != nil {
			return nil, err
		}
	}
	if err := dc.setup(ctx); err != nil {
		return nil, err
	}
	return dc, nil
}

func (dc *DChan) EnhanceSender(ctx context.Context, ch chan interface{}) error {
	q, err := dc.client.GetQueue(ctx, fmt.Sprintf(nameFmt, dc.name))
	if err != nil {
		return err
	}
	s := newSender(q, ch, dc.config)
	dc.mu.Lock()
	dc.senders = append(dc.senders, s)
	dc.mu.Unlock()
	return nil
}

func (dc *DChan) MakeSender(ctx context.Context) (chan<- interface{}, error) {
	q, err := dc.client.GetQueue(ctx, fmt.Sprintf(nameFmt, dc.name))
	if err != nil {
		return nil, err
	}
	ch := make(chan interface{})
	s := newSender(q, ch, dc.config)
	dc.mu.Lock()
	dc.senders = append(dc.senders, s)
	dc.mu.Unlock()
	return s.ch, nil
}

func (dc *DChan) EnhanceReceiver(ctx context.Context, ch chan interface{}) error {
	dc.mu.Lock()
	defer dc.mu.Unlock()
	q, err := dc.client.GetQueue(ctx, fmt.Sprintf(nameFmt, dc.name))
	if err != nil {
		return err
	}
	r := newReceiver(q, ch, dc.config)
	if atomic.LoadInt32(&dc.status) == closed {
		r.Close()
	}
	dc.receivers = append(dc.receivers, r)
	return nil
}

func (dc *DChan) MakeReceiver(ctx context.Context) (<-chan interface{}, error) {
	q, err := dc.client.GetQueue(ctx, fmt.Sprintf(nameFmt, dc.name))
	if err != nil {
		return nil, err
	}
	ch := make(chan interface{})
	r := newReceiver(q, ch, dc.config)
	dc.mu.Lock()
	dc.receivers = append(dc.receivers, r)
	dc.mu.Unlock()
	return r.ch, nil
}

func (dc *DChan) Close(ctx context.Context) error {
	if dc.closeAll() {
		if err := dc.topic.Publish(ctx, closedValue); err != nil {
			return err
		}
		//return dc.client.Shutdown(ctx)
	}
	return nil
}

func (dc *DChan) setup(ctx context.Context) error {
	topic, err := dc.client.GetTopic(ctx, fmt.Sprintf(nameFmt, dc.name))
	if err != nil {
		return err
	}
	var id types.UUID
	id, err = topic.AddMessageListener(ctx, func(event *hz.MessagePublished) {
		if event.Value == closedValue {
			if dc.closeAll() {
				// ignoring the error
				_ = dc.topic.RemoveListener(context.Background(), id)
			}
		}
	})
	dc.topic = topic
	return err
}

func (dc *DChan) closeAll() bool {
	if atomic.CompareAndSwapInt32(&dc.status, open, closed) {
		for _, r := range dc.receivers {
			r.Close()
		}
		for _, s := range dc.senders {
			s.Close()
		}
		return true
	}
	return false
}

type sender struct {
	ch     chan interface{}
	q      *hz.Queue
	config *config
	doneCh chan struct{}
}

func newSender(q *hz.Queue, ch chan interface{}, config *config) *sender {
	s := &sender{
		ch:     ch,
		q:      q,
		config: config,
		doneCh: make(chan struct{}),
	}
	go s.loop()
	return s
}

func (s *sender) Ch() chan<- interface{} {
	return s.ch
}

func (s *sender) Close() {
	defer func() {
		if v := recover(); v != nil {
			if err, ok := v.(error); ok {
				s.config.errorHandler(fmt.Errorf("recovered panic: %w", err))
			}
		}
	}()
	close(s.ch)
}

func (s *sender) Wait(ctx context.Context) {
	select {
	case <-ctx.Done():
		s.Close()
	case <-s.doneCh:
	}
}

func (s *sender) loop() {
loop:
	for {
		select {
		case v, ok := <-s.ch:
			if !ok {
				break loop
			}
			if err := s.q.Put(context.Background(), v); err != nil {
				s.config.errorHandler(fmt.Errorf("error putting: %w", err))
				break loop
			}
		}
	}
	close(s.doneCh)
}

type receiver struct {
	ch     chan interface{}
	q      *hz.Queue
	status int32
	config *config
}

func newReceiver(q *hz.Queue, ch chan interface{}, config *config) *receiver {
	r := &receiver{
		ch:     ch,
		q:      q,
		config: config,
	}
	go r.loop()
	return r
}

func (r *receiver) Ch() <-chan interface{} {
	return r.ch
}

func (r *receiver) Close() {
	if atomic.CompareAndSwapInt32(&r.status, open, closed) {
		//close(r.ch)
	}
}

func (r *receiver) loop() {
	defer func() {
		if v := recover(); v != nil {
			if err, ok := v.(error); ok {
				r.config.errorHandler(fmt.Errorf("recovered panic: %w", err))
			}
		}
	}()
	for {
		v, err := r.q.PollWithTimeout(context.Background(), r.config.pollTimeout)
		if err != nil {
			r.config.errorHandler(fmt.Errorf("error taking: %w", err))
			break
		}
		if v == nil {
			if atomic.LoadInt32(&r.status) == closed {
				break
			}
			continue
		}
		//if atomic.LoadInt32(&r.status) == open {
		r.ch <- v
		//}
	}
	close(r.ch)
}
